econodrone
==========

Source code for the econodrone.co.uk website.


To Do
-----

v 1.0.2refactor:

- [x] Refactor css (C3CSS)
- [x] Small improvements to design and layout
- [x] Split CSS files
- [ ] Redesign sticky header

Ideas:

- [ ] Redesign how the single image pages are built
- [ ] Add sourcemaps

Assets
------

* Icons sourced from https://feathericons.com/
* Using [Noto Serif](https://fonts.google.com/specimen/Noto+Serif) font