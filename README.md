Template - TM01
==================

A very basic project template for starting a website.

Running on NodeJS, it's basically a Gulp script that compiles the website, plus some starter template files.

* NodeJS
* Gulp
* handlebarsjs
* scss

To run:

 npm run dev

This will run the gulp script, which watches for changes to files and updates the build script.

 npm start

This starts up a development server.

Options:

 npm run sitemap

This sets up a sitemap. Update gulpfile options and robots.txt if using.