window.onload = (event) => {
  /* header is designed to shrink on scroll */
  const header = document.querySelector('.header');

  const stickyControl = () => {
    console.log(window.pageYOffset);
    if (window.pageYOffset > 100) {
      header.classList.add('shrunk');
      header.classList.add('sticky');
    } else if (window.pageYOffset <= 50) {
      header.classList.remove('shrunk');
      header.classList.remove('sticky');
    }
  }

  // let timer = window.setTimeout(stickyControl, 10000);
  let timer;

  window.onscroll = () => {
    // if (timer) {
    //   window.clearTimeout(timer);
    // }
    timer = window.setTimeout(stickyControl, 100);
  };
};