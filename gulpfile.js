const gulp = require('gulp');
const del = require('del');
const sass = require('gulp-sass');
const fs = require('fs');
const data = require('gulp-data');
const handlebars = require('gulp-compile-handlebars');
const htmlBeautify = require('gulp-html-beautify');
const rename = require('gulp-rename');
const sitemap = require('gulp-sitemap');

function clean(cb) {
    del(['./dist/*']);
    console.log("Cleaned");
    cb();
}

/** CSS - SASS files compiled into css files */
function css(cb) {
    gulp.src('src/css/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('./dist/css'));
    cb();
}

/** HMTL - handlebars templates compiled into HTML files */

function html(cb) {
    let date = new Date();
    let templateData = JSON.parse(fs.readFileSync('./src/data/tr-data.json'));
    templateData.lastUpdated = date.toDateString();

    // Options
    let options = {
        ignorePartials: true,
        batch: ['./src/templates/partials']
    };

    // Build it
    gulp.src('src/templates/*.hbs')
        .pipe(handlebars(templateData, options))
        .pipe(rename(function (path) {
            path.extname = '.html';
        }))
        .pipe(htmlBeautify({
            indentSize: 2
        }))
        .pipe(gulp.dest('./dist'));
    console.log("build html");
    cb();
}

/** Compile Javascript files */
function js(cb) {
    gulp.src('./src/js/*.js')
        .pipe(gulp.dest('./dist/scripts'));
    cb();
}

/* Move images etc */
function images(cb) {
    gulp.src('./src/images/*')
        .pipe(gulp.dest('./dist/images'));
    gulp.src('./src/media/*')
        .pipe(gulp.dest('./dist/media'));
    gulp.src('./src/fonts/*')
        .pipe(gulp.dest('./dist/fonts'));
    gulp.src('./src/robots.txt')
        .pipe(gulp.dest('./dist'));
    cb();
}


/* Build sitemap */
// function sm(cb) {
//     gulp.src('./dist/**/*.html', {
//             read: false
//         })
//         .pipe(sitemap({
//             siteUrl: 'my url',
//             // changefreq: 'monthly',
//             priority: function (siteUrl, loc, entry) {
//                 // Give pages inside root path (i.e. no slashes) a higher priority
//                 console.log(loc);
//                 return loc === siteUrl ? 1 : 0.5;
//             }
//         }))
//         .pipe(gulp.dest('./dist'));
//     cb();
// }

/** Watch files */
gulp.watch('./src/css/*.scss', css);
gulp.watch('./src/js/*.js', js);
gulp.watch(['./src/templates/**/*.hbs', './src/data/*.json'], html);

exports.default = gulp.series(clean);
exports.dev = gulp.series(clean, html, css, js, images);
// exports.sm = sm;